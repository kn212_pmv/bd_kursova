import {$authHost, $host} from "./index";
import jwt_decode from "jwt-decode"

export const createType = async (type) => {
    const {data} = await $authHost.post('/type', type)
    return data
}

export const fetchTypes = async () => {
    const {data} = await $host.get('/type')
    return data
}

export const createCity = async (city) => {
    const {data} = await $authHost.post('/city', city)
    return data
}

export const fetchCities = async () => {
    const {data} = await $host.get('/city')
    return data
}

export const createApartment = async (apartment) => {
    const {data} = await $authHost.post('/apartment', apartment)
    return data
}

export const fetchApartments = async (typeId, cityId, page, limit = 5) => {
    const {data} = await $host.get('/apartment', {
        params: {
            typeId, cityId, page, limit
        }
    })
    return data
}

export const fetchOneApartment = async (id) => {
    const {data} = await $host.get('/apartment/' + id)
    return data
}

export const deleteApartment = async (id) => {
    const {data} = await $authHost.delete('/apartment/' + id)
    console.log(id)
    return data
}


export const addToFavourite = async (apartmentId) => {
    const {response} = await $authHost.post('/favourite', apartmentId)
    return response
}

export const getFavourite = async () => {
    const {data} = await $authHost.get('/favourite')
    return data
}

export const deleteAllFavourite = async () => {
    const {data} = await $authHost.delete('/favourite/delete')
    return data
}

export const deleteFavourite = async (id) => {
    const {data} = await $authHost.delete('/favourite/delete/' + id)
    return data
}

export const createBackup = async () => {
    const {data} = await $authHost.post('/backup');
    return data
};

