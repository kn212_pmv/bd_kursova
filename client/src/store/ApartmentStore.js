import {makeAutoObservable} from "mobx";

export default class ApartmentStore {
    constructor() {
        this._types = []
        this._cities = []
        this._apartments = []
        this._selectedType = {}
        this._selectedCity = {}
        this._favourites = []
        this._page = 1
        this._totalCount = 0
        this._limit = 3
        makeAutoObservable(this)
    }

    setTypes(types) {
        this._types = types
    }

    setFavourites(favourite) {
        this._favourites = favourite
    }

    get favourites() {
        return this._favourites
    }

    setCities(cities) {
        this._cities = cities
    }

    setApartments(apartments) {
        this._apartments = apartments
    }

    setSelectedType(type) {
        this._selectedType = type

    }

    setPage(page) {
        this._page = page

    }
    setTotalCount(count) {
        this._totalCount = count

    }

    setSelectedCity(city) {
        this._selectedCity = city

    }

    get types() {
        return this._types
    }

    get cities() {
        return this._cities
    }

    get apartments() {
        return this._apartments
    }

    get selectedType() {
        this.setPage(1)
        return this._selectedType
    }

    get selectedCity() {
        this.setPage(1)
        return this._selectedCity
    }

    get page() {
        return this._page
    }
    get totalCount() {
        return this._totalCount
    }
    get limit() {
        return this._limit
    }
}