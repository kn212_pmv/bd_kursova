import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Card, Row} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';


const CityBar = observer(() => {
    const {apartment} = useContext(Context)
    return (
        <Row className="d-flex">
            <div className="d-flex flex-row mb-3 p-3">


                {apartment.cities.map(city =>
                    <Card
                        key={city.id}
                        style={{cursor: 'pointer'}}
                        className="p-3"
                        onClick={() => apartment.setSelectedCity(city)}
                        border={city.id === apartment.selectedCity.id ? 'danger': 'light'}



                    >
                        {city.name}


                    </Card>
                )}
            </div>


        </Row>
    );
});

export default CityBar;