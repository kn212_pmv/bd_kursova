import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {ListGroup, ListGroupItem} from "react-bootstrap";

const TypeBar = observer(() => {
    const {apartment} = useContext(Context)
    return (
        <ListGroup className="m-2">
            {apartment.types.map(type =>
                <ListGroupItem
                    style={{cursor: 'pointer'}}
                    active={type.id === apartment.selectedType.id}
                    onClick={() => apartment.setSelectedType(type)}
                    key={type.id}>
                    {type.name}
                </ListGroupItem>
            )}
        </ListGroup>
    );
});

export default TypeBar;