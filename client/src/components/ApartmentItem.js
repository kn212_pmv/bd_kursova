import React from 'react';
import {Card, Col} from "react-bootstrap";
import {APARTMENT_ROUTE} from "../utils/consts";
import {useNavigate} from 'react-router-dom'
import Image from 'react-bootstrap/Image'

const ApartmentItem = ({apartment}) => {
    const navigate = useNavigate()



    return (
        <Col md={3} className={"mt-3"} onClick={() => navigate(APARTMENT_ROUTE + '/' + apartment.id)}>
            <Card style={{width: 150, cursor: 'pointer'}} border={"light"}>
                <Image width={250} height={150} src={process.env.REACT_APP_API_URL + apartment.img}/>
                <div className="text-black-50 mt-1 d-flex justify-content-between align-items-center">
                    <div>Місто</div>
                </div>
                <div>{apartment.address}</div>
            </Card>
        </Col>


    );
};

export default ApartmentItem;