import React, {useContext, useEffect} from 'react';
import {Context} from "../index";
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Button} from "react-bootstrap";
import {observer} from "mobx-react-lite";
import {useNavigate} from 'react-router-dom'
import {ADMIN_ROUTE, FAVOURITES_ROUTE, LOGIN_ROUTE} from "../utils/consts";

const NavBar = observer(() => {
    const {user} = useContext(Context)
    const history = useNavigate()



    const logOut = () => {
        user.setUser({})
        user.setIsAuth(false)

    }
    return (
        <Navbar bg="primary" variant="dark">

            <Container>
                <Navbar.Brand href="/">HouseRental</Navbar.Brand>
                {user.isAuth ?
                    <Nav className="ml-auto mr-2">
                        <h2>{user.user} </h2>
                        {user.isAdmin ?


                        <Button variant="dark" onClick={() => history(ADMIN_ROUTE)}>Адмін панель</Button>
                            :
                            <div> </div>
                        }
                        <Button variant="dark" onClick={() => history(FAVOURITES_ROUTE)} >Улюблене</Button>
                        <Button variant="success" onClick={() => logOut()}>Вийти</Button>
                    </Nav>
                    :

                    <Nav className="ml-auto">
                        <Button variant="success" onClick={() => history(LOGIN_ROUTE)}>Авторизація</Button>
                    </Nav>
                }
            </Container>
        </Navbar>
    );

});

export default NavBar;