import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Pagination} from "react-bootstrap";

const Pages = observer(() => {
    const {apartment} = useContext(Context)
    const pageCount = Math.ceil(apartment.totalCount / apartment.limit)
    const pages = []

    for (let i = 0; i < pageCount; i++) {
        pages.push(i + 1)
    }
    return (
        <Pagination className="mt-5">
            {pages.map(page =>
                <Pagination.Item
                    key={page}
                    active={apartment.page === page }

                    onClick={() => apartment.setPage(page)}
                >{page}</Pagination.Item>
            )}


        </Pagination>
    );
});

export default Pages;