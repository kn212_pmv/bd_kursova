import React, {useState} from 'react';
import {Button, Form, FormControl, Modal} from "react-bootstrap";
import data from "bootstrap/js/src/dom/data";
import {set} from "mobx";
import {createType} from "../../http/apartmentAPI";

const CreateType = ({show, onHide}) => {

    const [value, setValue] = useState('')

    const addType = () => {
        createType({name: value}).then(data => {
            setValue('')
            onHide()

        })


    }
    return (
        <Modal

            show={show}
            onHide={onHide}
            size='lg'

            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Додати новий тип
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <FormControl
                        placeholder={"Введіть назву"}
                        value={value}
                        onChange={e => setValue(e.target.value)}
                    >

                    </FormControl>

                </Form>

            </Modal.Body>
            <Modal.Footer>
                <Button variant={'outline-danger'} onClick={onHide}>Закрити</Button>
                <Button variant={'outline-success'} onClick={addType}>Додати</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateType;