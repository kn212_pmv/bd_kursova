import { Modal, Button, Spinner } from "react-bootstrap";
import { useState } from "react";
import { createBackup } from "../../http/apartmentAPI";

const BackupModal = ({ show, onHide }) => {
    const [isLoading, setIsLoading] = useState(false);
    const [backupMessage, setBackupMessage] = useState("");

    const handleBackup = () => {
        setIsLoading(true);
        setBackupMessage("");

        createBackup()
            .then((response) => {
                setIsLoading(false);
                setBackupMessage("Бекап створено успішно!");
            })
            .catch((error) => {
                setIsLoading(false);
                setBackupMessage("Помилка при створенні бекапу: " + error.message);
                console.error(error);
            });
    };

    return (
        <Modal show={show} onHide={onHide}>
            <Modal.Header closeButton>
                <Modal.Title>Створення бекапу</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {backupMessage && (
                    <div className={backupMessage.includes("Помилка") ? "text-danger" : ""}>
                        {backupMessage}
                    </div>
                )}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>
                    Закрити
                </Button>
                <Button variant="primary" onClick={handleBackup} disabled={isLoading}>
                    {isLoading ? (
                        <>
                            <Spinner animation="border" size="sm" /> Зачекайте...
                        </>
                    ) : (
                        "Створити бекап"
                    )}
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default BackupModal;