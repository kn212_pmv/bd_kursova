import React, {useContext, useEffect, useState} from 'react';
import {Button, Col, Dropdown, Form, FormControl, Modal, Row} from "react-bootstrap";
import {Context} from "../../index";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import DropdownMenu from "react-bootstrap/DropdownMenu";
import DropdownItem from "react-bootstrap/DropdownItem";
import {createApartment, fetchApartments, fetchCities, fetchTypes} from "../../http/apartmentAPI";
import {observer} from "mobx-react-lite";

const CreateApartment = observer(({show, onHide}) => {
            const {apartment} = useContext(Context)
            const [address, setAddress] = useState('')
            const [price, setPrice] = useState('')
            const [area, setArea] = useState('')
            const [rooms, setRooms] = useState('')
            const [file, setFile] = useState(null)
            const [info, setInfo] = useState([])

            useEffect(() => {
                fetchTypes().then(data => apartment.setTypes(data))
                fetchCities().then(data => apartment.setCities(data))

            }, [])

            const addInfo = () => {
                setInfo([...info, {title: '', description: '', number: Date.now()}])
            }

            const changeInfo = (key, value, number) => {
                setInfo(info.map(i => i.number === number ? {...i, [key]: value} : i))
            }
            const removeInfo = (number) => {
                setInfo(info.filter(i => i.number !== number))
            }

            const selectFile = e => {
                setFile(e.target.files[0])
            }

            const addApartment = () => {
                const formData = new FormData()
                formData.append('address', address)
                formData.append('price', `${price}`)
                formData.append('img', file)
                formData.append('area', `${area}`)
                formData.append('rooms', `${rooms}`)
                formData.append('typeId', apartment.selectedType.id)
                formData.append('cityId', apartment.selectedCity.id)
                formData.append('info', JSON.stringify(info))
                createApartment(formData).then(data => onHide())


            }


            return (
                <Modal

                    show={show}
                    onHide={onHide}
                    size='lg'

                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                            Додати нове житло
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Dropdown className="mt-2 mb-2">
                                <DropdownToggle>
                                    {apartment.selectedType.name || "Оберіть тип"}
                                </DropdownToggle>
                                <DropdownMenu className="mt-2 mb-2">
                                    {apartment.types.map(type => {
                                        return <Dropdown.Item onClick={() => apartment.setSelectedType(type)}
                                                              key={type.id}>{type.name}</Dropdown.Item>
                                    })}
                                </DropdownMenu>
                            </Dropdown>
                            <Dropdown className="mt-2 mb-2">
                                <DropdownToggle>
                                    {apartment.selectedCity.name || "Оберіть місто"}
                                </DropdownToggle>
                                <DropdownMenu>
                                    {apartment.cities.map(type => {
                                        return <Dropdown.Item onClick={() => apartment.setSelectedCity(type)}
                                                              key={type.id}>{type.name}</Dropdown.Item>
                                    })}
                                </DropdownMenu>
                            </Dropdown>
                            <FormControl
                                value={address}
                                onChange={e => setAddress(e.target.value)}
                                className="mt-3"
                                placeholder='Введіть адресу'>
                            </FormControl>
                            <FormControl
                                placeholder='Введіть ціну оренди'
                                value={price}
                                onChange={e => setPrice(Number(e.target.value))}
                                className="mt-3">

                            </FormControl>
                            <FormControl
                                placeholder='Введіть площу'
                                value={area}
                                onChange={e => setArea(Number(e.target.value))}
                                className="mt-3"
                            >
                            </FormControl>
                            <FormControl
                                placeholder='Введіть кількість кімнат'
                                value={rooms}
                                onChange={e => setRooms(Number(e.target.value))}
                                className="mt-3"
                            >
                            </FormControl>
                            <FormControl
                                type='file'
                                className="mt-3"
                                onChange={selectFile}
                            >
                            </FormControl>
                            <hr/>
                            <Button
                                variant="outline-dark"
                                onClick={addInfo}
                            >
                                Додати нове житло
                            </Button>
                            {info.map((i) => (
                                <Row className="mt-4" key={i.number}>
                                    <Col md={4}>

                                        <FormControl placeholder="Введіть назву характеристики"
                                                     value={i.title}
                                                     onChange={(e) => changeInfo('title', e.target.value, i.number)}
                                        />
                                    </Col>
                                    <Col md={4}>
                                        <FormControl placeholder="Введіть опис"
                                                     value={i.description}
                                                     onChange={(e) => changeInfo('description', e.target.value, i.number)}
                                        />
                                    </Col>
                                    <Col md={4}>
                                        <Button
                                            variant="outline-danger"
                                            onClick={() => removeInfo(i.number)}
                                        >
                                            Видалити
                                        </Button>

                                    </Col>
                                </Row>
                            ))
                            }


                        </Form>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant={'outline-danger'} onClick={onHide}>Закрити</Button>
                        <Button variant={'outline-success'} onClick={addApartment}>Додати</Button>
                    </Modal.Footer>
                </Modal>
            );
        }
    )
;

export default CreateApartment;