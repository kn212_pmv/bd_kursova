import React, {useState} from 'react';
import {Button, Form, FormControl, Modal} from "react-bootstrap";
import {createCity} from "../../http/apartmentAPI";

const CreateCity = ({show, onHide}) => {
    const [value, setValue] = useState('')

    const addCity = () => {
        createCity({name: value}).then(data => {
            setValue('')
            onHide()

        })


    }
    return (
        <Modal

            show={show}
            onHide={onHide}
            size='lg'

            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Додати місто
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <FormControl placeholder={"Введіть назву"}
                                 value={value}
                                 onChange={e => setValue(e.target.value)}>

                    </FormControl>

                </Form>

            </Modal.Body>
            <Modal.Footer>
                <Button variant={'outline-danger'} onClick={onHide}>Закрити</Button>
                <Button variant={'outline-success'}   onClick={addCity}>Додати</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default CreateCity;