import React, {useContext} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {Row} from "react-bootstrap";
import ApartmentItem from "./ApartmentItem";

const ApartmentList = observer(() => {
    const {apartment} = useContext(Context)

    return (
        <Row className='d-flex'>

                {apartment.apartments.map(apartment =>
                    <ApartmentItem key={apartment.id} apartment={apartment}/>



                )}






        </Row>
    );
});

export default ApartmentList;