import React, {useContext, useEffect} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import TypeBar from "../components/TypeBar";
import CityBar from "../components/CityBar";
import ApartmentList from "../components/ApartmentList";
import {observer} from "mobx-react-lite";
import {Context} from "../index";
import {fetchApartments, fetchCities, fetchTypes} from "../http/apartmentAPI";
import Pages from "../components/Pages";


const Shop = observer(() => {
    const {apartment} = useContext(Context)

    useEffect(() => {
        fetchTypes().then(data => apartment.setTypes(data))
        fetchCities().then(data => apartment.setCities(data))
        fetchApartments(null,null,1,2).then(data => {
            apartment.setApartments(data.rows)
            apartment.setTotalCount(data.count)
        })
    }, [])

    useEffect(() => {
        fetchApartments(apartment.selectedType.id,apartment.selectedCity.id, apartment.page, 5).then(data =>{
            apartment.setApartments(data.rows)
            apartment.setTotalCount(data.count)
        })

    }, [apartment.page, apartment.selectedType, apartment.selectedCity])
    return (
        <Container>
            <Row className="d-flex flex-row ">
                <Col md={3}>
                    <TypeBar/>

                </Col>
                <Col md={9}>
                    <CityBar/>
                    <ApartmentList/>
                    <Pages/>

                </Col>
            </Row>
        </Container>
    );
});

export default Shop;