import React, {useEffect, useState} from 'react';
import {Button, Card, Col, Container, Image, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {addToFavourite, deleteApartment, fetchOneApartment,} from "../http/apartmentAPI";

const ApartmentPage = () => {
    const [apartment, setApartment] = useState({info: []})
    const {id} = useParams()


    useEffect(() => {
        fetchOneApartment(id).then(data => setApartment(data))

    }, [])
    const description = [
        {id: 1, title: 'Опис', description: 'Квартира сучна, з ремонтом. Номер власника: +380941233123'}

    ]
    const add = () => {
        const formData = new FormData()
        formData.append('appartmentId', id)
        addToFavourite(formData).then(response => alert('Додано до улюбленого'))
    }

    const deleteItem = () => {
        deleteApartment(id).then(response => alert(`Успішно видалено!`))
    }


    return (
        <Container className='mt-3'>
            <Row>
                <Col>
                    <Image style={{
                        maxWidth: "100%",
                        maxHeight: "100%",
                        objectFit: "contain",
                        fontSize: 22,
                        border: "5px solid lightgray"
                    }}
                           src={process.env.REACT_APP_API_URL + apartment.img}/>

                </Col>
                <Col>
                    <Card
                        className="d-flex flex-column align-items-center justify-content-around"
                        style={{width: 400, height: 400, fontSize: 22, border: "5px solid lightgray"}}
                    >
                        <div>
                            Ціна оренди: <strong>{apartment.price} грн.</strong>
                        </div>
                        <div>
                            Адреса: <strong>{apartment.address}</strong>
                        </div>
                        <div>
                            Площа: <strong>{apartment.area} м/кв</strong>
                        </div>
                        <div>
                            Кількість кімнат: <strong>{apartment.rooms}</strong>
                        </div>
                        <div className='ms-4 me-4 d-flex' style={{fontSize: 18}}>
                            {apartment.info.map(info =>
                                <Row key={info.id}>
                                    {info.title}: {info.description}

                                </Row>
                            )}
                        </div>
                        <Button variant={"outline-dark"} onClick={add}>Додати в улюблене</Button>
                        <Button variant={"outline-danger"} onClick={deleteItem}>Видалити</Button>
                    </Card>


                </Col>


            </Row>

        </Container>
    );
};

export default ApartmentPage;