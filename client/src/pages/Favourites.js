import React, {useContext, useEffect} from 'react';
import {Button, Card, Col, Container, Row} from 'react-bootstrap'
import {observer} from 'mobx-react-lite';
import {Context} from "../index";
import {deleteAllFavourite, deleteFavourite, getFavourite} from "../http/apartmentAPI";


const Favourites = observer(() => {
    const {apartment} = useContext(Context)

    useEffect(() => {
        getFavourite().then(data => {
            apartment.setFavourites(data)
        })


    }, [])

    const deleteAll = () => {
        deleteAllFavourite().then(response => alert(`Очищено!`))
        getFavourite().then(data => {
            apartment.setFavourites(data)
        })
    }

    const deleteOne = (id) => {
        deleteFavourite(id).then(response => alert(`Очищено!`))
        getFavourite().then(data => {
            apartment.setFavourites(data)
        })
    }






    // ----- Считаем общую сумму, которую юзер набрал в корзину ------- //

    let prices = 0;
    {
        apartment.favourites.map(price =>
            prices += Number(price.appartment.price)
        )
    }
    return (

        <Container
            className="d-flex flex-sm-column justify-content-center align-items-center mt-3"
        >
            <h1 className="pb-2">Обрані</h1>


            {/* ------- Считаем общую сумму ------- */}




            {apartment.favourites.map(product =>

                <Card className="d-flex w-100 p-2 justify-content-center mb-2" key={product.id}>
                    <Row className="d-flex w-100">
                        <Col>
                            <div className="d-flex flex-row align-items-center">
                                <img src={process.env.REACT_APP_API_URL + product.appartment.img} width={50}/>
                                <h1 className="pl-3">{product.appartment.address}</h1>
                            </div>
                        </Col>
                        <Col>
                            <div className="d-flex h-100 flex-row justify-content-end align-items-center">
                                <h2 className="font-weight-light">{product.appartment.price} грн.</h2>
                            </div>
                        </Col>


                    </Row>


                </Card>

            )}
            <Button variant={'outline-dark'} className='mt-2' onClick={deleteAll} >Очистити</Button>
        </Container>


    );
});

export default Favourites;