import React, {useContext, useState} from 'react';
import {Container, Form, Button, Card, Row} from 'react-bootstrap';
import {LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "../utils/consts";
import {NavLink, useLocation, useNavigate} from 'react-router-dom'
import {registration, login} from "../http/userAPI";
import {observer} from "mobx-react-lite";
import {Context} from "../index";

const Auth = observer(() => {
    const {user} = useContext(Context)
    const location = useLocation()
    const navigate = useNavigate()
    const isLogin = location.pathname === LOGIN_ROUTE
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const click = async () => {
        try {


            let data;
            if (isLogin) {
                data = await login(email, password);
            } else {
                data = await registration(email, password)
            }
            user.setUser(data);
            //user.setIsAuth(true)
             if (email === "react4@gmail.com"){
                 user.setIsAdmin(true)
             }


            navigate(SHOP_ROUTE)


        } catch (e) {
            alert(e.response.data.message)
        }
    }


    return (
        <Container className="d-flex justify-content-center align-items-center"
                   style={{height: window.innerHeight - 54}}>
            <Card style={{width: 600}} className="p-5">
                <h2 className="m-auto">{isLogin ? 'Вхід на сайт' : 'Реєстрація'}</h2>
                <main>
                    <Form method="post" action="">
                        <Form.Group className="mb-3" controlId="login">
                            <Form.Label>Логін</Form.Label>
                            <Form.Control
                                placeholder="login"
                                value={email}
                                onChange={e => setEmail(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="password">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control type="password"
                                          name="password"
                                          placeholder="Password"
                                          value={password}
                                          onChange={e => setPassword(e.target.value)}/>
                        </Form.Group>
                        <Row className="d-flex justify-content-between mt-3 p-3">
                            {isLogin ?
                                <div>
                                    Немає аккаунта? <NavLink to={REGISTRATION_ROUTE}>Зареєструватись!</NavLink>
                                </div>
                                :
                                <div>
                                    Є аккаунт? <NavLink to={LOGIN_ROUTE}>Увійти!</NavLink>
                                </div>
                            }
                            <Button variant="primary" onClick={click}
                                    className="w-100 mt-2">
                                {isLogin ?
                                    'Увійти'
                                    :
                                    'Рєстрація'}
                            </Button>
                        </Row>

                    </Form>
                </main>
            </Card>
        </Container>
    );
});

export default Auth;



// import React, {useContext, useState} from 'react';
// import {Button, Card, Container, Form} from "react-bootstrap";
// import {LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE} from "../utils/consts";
// import {NavLink, useLocation, useNavigate} from "react-router-dom";
// import {login, registration} from "../http/userAPI";
// import {observer} from "mobx-react-lite";
// import {Context} from "../index";
//
// const Auth = observer(() => {
//     const {user} = useContext(Context)
//     const location = useLocation()
//     const navigate = useNavigate()
//     const isLogin = location.pathname === LOGIN_ROUTE
//     const [email, setEmail] = useState('')
//     const [password, setPassword] = useState('')
//
//     const click = async () => {
//         try{
//             let data;
//             if (isLogin) {
//                 data = await login(email, password)
//             } else {
//                 data = await registration(email, password)
//             }
//             user.setUser(data)
//              if(user.user.role === 'ADMIN')
//                  user.setIsAdmin(true)
//              else
//                  user.setIsAdmin(false)
//
//             console.log(user.isAdmin)
//             console.log(user.user)
//
//
//             //user.setIsAuth(true)
//
//
//
//             // console.log(user)
//
//
//
//             navigate(SHOP_ROUTE)
//         }catch (e){
//             alert(e.response.data.message)
//         }
//     }
//     return (
//         <Container className="d-flex justify-content-center align-items-center"
//                    style={{height: window.innerHeight - 54}}>
//             <Card style={{width: 600, backgroundColor: "lightcyan"}} className="p-5">
//                 <h2 className="text-center">{isLogin ? 'Авторизація' : 'Реєстрація'}</h2>
//                 <Form>
//                     <Form.Group className="mb-3" controlId="formBasicEmail">
//
//                         <Form.Control type="email"
//                                       placeholder="Введіть пошту"
//                                       value={email}
//                                       onChange={e => setEmail(e.target.value)}/>
//
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formBasicPassword">
//
//                         <Form.Control type="password"
//                                       placeholder="Введіть пароль"
//                                       value={password}
//                                       onChange={e => setPassword(e.target.value)}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3">
//                         {isLogin ?
//                             <div>
//                                 <span style={{display: 'inline'}}>Немає аккаунта? </span>
//                                 <NavLink to={REGISTRATION_ROUTE}
//                                          style={{display: 'inline', color: "blue"}}>Реєстрація</NavLink>
//                             </div>
//                             :
//                             <div>
//                                 <span style={{display: 'inline'}}>Вже зареєстровані? </span>
//                                 <NavLink to={LOGIN_ROUTE}
//                                          style={{display: 'inline', color: "green"}}>Увійдіть</NavLink>
//                             </div>
//                         }
//                     </Form.Group>
//                     <Button variant="dark" className="justify-content-center" onClick={click}>
//                         {isLogin ?
//                             'Увійти'
//                             :
//                             'Зареєструватися'
//                         }
//                     </Button>
//                 </Form>
//             </Card>
//         </Container>
//     );
// });
//
// export default Auth;
