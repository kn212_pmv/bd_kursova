import React, {useState} from 'react';
import {Button, Container} from "react-bootstrap";
import CreateType from "../components/modals/CreateType";
import CreateApartment from "../components/modals/CreateApartment";
import CreateCity from "../components/modals/CreateCity";
import BackupModal from "../components/modals/Backup";

const Admin = () => {
    const [cityVisible, setCityVisible] = useState(false)
    const [typeVisible, setTypeVisible] = useState(false)
    const [apartmentVisible, setApartmentVisible] = useState(false)
    const [backupVisible, setBackupVisible] = useState(false)

    return (
        <Container className='d-flex flex-column'>
            <Button variant={'outline-dark'} className='mt-2' onClick={() => setTypeVisible(true)}>Додати тип</Button>
            <Button variant={'outline-dark'} className='mt-2' onClick={() => setCityVisible(true)}>Додати місто</Button>
            <Button variant={'outline-dark'} className='mt-2' onClick={() => setApartmentVisible(true)}>Додати житло</Button>
            <Button variant={"outline-info"} className="mt-3" onClick={() => setBackupVisible(true)}>Створити бекап</Button>
            <CreateType show={typeVisible} onHide={() => setTypeVisible(false)}/>
            <CreateApartment show={apartmentVisible} onHide={() => setApartmentVisible(false)}/>
            <CreateCity show={cityVisible} onHide={() => setCityVisible(false)}/>
            <BackupModal show={backupVisible} onHide={() => setBackupVisible(false)} />


        </Container>
    );
};

export default Admin;