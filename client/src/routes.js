import {
    ADMIN_ROUTE,
    APARTMENT_ROUTE,
    FAVOURITES_ROUTE,
    LOGIN_ROUTE,
    REGISTRATION_ROUTE,
    SHOP_ROUTE
} from "./utils/consts";
import Favourites from "./pages/Favourites";
import Admin from "./pages/Admin";
import ApartmentPage from "./pages/ApartmentPage";
import Auth from "./pages/Auth";
import Shop from "./pages/Shop";

export const authRoutes = [
    {
        path: ADMIN_ROUTE,
        Component: Admin
    },
    {
        path: FAVOURITES_ROUTE,
        Component: Favourites
    }


]

export const publicRoutes = [
    {
        path: SHOP_ROUTE,
        Component: Shop
    },
    {
        path: LOGIN_ROUTE,
        Component: Auth
    },
    {
        path: REGISTRATION_ROUTE,
        Component: Auth
    },
    {
        path: APARTMENT_ROUTE + '/:id',
        Component: ApartmentPage
    }

]