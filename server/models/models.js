const sequelize = require('../db')

const {DataTypes} = require('sequelize')

const User = sequelize.define('user', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    email: {type: DataTypes.STRING, unique: true},
    password: {type: DataTypes.STRING},
    role: {type: DataTypes.STRING, defaultValue: "USER"},
})

const Favourite = sequelize.define('favourite', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},

})

const FavouriteApartment = sequelize.define('favourite_apartment', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},

})

const Apartment = sequelize.define('appartment', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    address: {type: DataTypes.STRING, unique: true, allowNull: false},
    area: {type: DataTypes.INTEGER, allowNull: false},
    rooms: {type: DataTypes.INTEGER, allowNull: false},
    price: {type: DataTypes.INTEGER, allowNull: false},
    img: {type: DataTypes.STRING, allowNull: false},
})

const Type = sequelize.define('type', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, unique: true, allowNull: false},
})

const City = sequelize.define('city', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, unique: true, allowNull: false},
})

const ApartmentInfo = sequelize.define('apartment_info', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    title: {type: DataTypes.STRING, allowNull: false},
    description: {type: DataTypes.STRING, allowNull: false},
})

const TypeCity = sequelize.define('type_city', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},

})

User.hasOne(Favourite)
Favourite.belongsTo(User)

Favourite.hasMany(FavouriteApartment)
FavouriteApartment.belongsTo(Favourite)

Type.hasMany(Apartment)
Apartment.belongsTo(Type)

City.hasMany(Apartment)
Apartment.belongsTo(City)

Apartment.hasMany(FavouriteApartment)
FavouriteApartment.belongsTo(Apartment)

Apartment.hasMany(ApartmentInfo, {as:'info'});
ApartmentInfo.belongsTo(Apartment)

Type.belongsToMany(City, {through: TypeCity})
City.belongsToMany(Type, {through: TypeCity})


module.exports = {
    User,
    Favourite,
    FavouriteApartment,
    Apartment,
    Type,
    City,
    TypeCity,
    ApartmentInfo

}




