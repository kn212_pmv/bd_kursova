const Router = require('express')
const router = new Router()

const favouriteController = require('../controllers/favouriteController')

// ------- Добавил проверку на авторизацию для того, чтобы вытащить оттуда авторизованного юзера -------- //
const authMiddleware = require('../middleware/authMiddleware')

// ------- CRUD корзины ------- //
router.get('/', authMiddleware, favouriteController.getFavouriteUser)
router.post('/', authMiddleware, favouriteController.addToFavourite)
router.delete('/delete', authMiddleware, favouriteController.removeAllFromFavourite)
router.delete('/delete/:id', authMiddleware, favouriteController.removeFromFavourite)


module.exports = router