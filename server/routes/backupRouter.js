const Router = require('express')
const router = new Router()
const backupController = require('../controllers/backupController')
const checkRole = require('../middleware/CheckRoleMiddleware')

router.post('/', checkRole('ADMIN'), backupController.createBackup)

module.exports = router