const Router = require('express')
const router = new Router()
const apartmentRouter = require('./apartmentRouter')
const cityRouter = require('./cityRouter')
const typeRouter = require('./typeRouter')
const userRouter = require('./userRouter')
const favouriteRouter = require('./favouriteRouter')
const backupRouter = require('./backupRouter')


router.use('/user', userRouter)
router.use('/type', typeRouter)
router.use('/apartment', apartmentRouter)
router.use('/city', cityRouter)
router.use('/favourite', favouriteRouter)
router.use('/backup', backupRouter)

module.exports = router
