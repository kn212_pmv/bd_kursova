const Router = require('express')
const router = new Router()
const apartmentController = require('../controllers/apartmentController')
const checkRole = require('../middleware/checkRoleMiddleware')


router.post('/', checkRole('ADMIN'), apartmentController.create)
router.get('/', apartmentController.getAll)
router.get('/:id', apartmentController.getOne)
router.delete('/:id', checkRole('ADMIN'), apartmentController.delete)
router.patch('/:id', checkRole('ADMIN'), apartmentController.update)

module.exports = router