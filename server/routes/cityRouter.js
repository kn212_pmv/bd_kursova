const Router = require('express')
const router = new Router()
const cityController = require('../controllers/cityController')
const checkRole = require('../middleware/checkRoleMiddleware')


router.post('/',checkRole('ADMIN'),cityController.create)
router.get('/',cityController.getAll)
router.delete('/delete/:id',checkRole('ADMIN'),cityController.delete)
router.patch('/update/:id',checkRole('ADMIN'),cityController.update)

module.exports = router
