const {Device, BasketDevice, Basket, FavouriteApartment, Apartment} = require("../models/models")

class FavouriteController {
    // ------ CRUD корзины ------ //

    async addToFavourite(req, res, next) {
        const user = req.user
        const {appartmentId} = req.body
        const basket = await FavouriteApartment.create({favouriteId: user.id, appartmentId: appartmentId})
        return res.json(basket)
    }

    async getFavouriteUser(req, res) {
        const {id} = req.user
        const basket = await FavouriteApartment.findAll({
            include: {
                model: Apartment
            }, where: {favouriteId: id}
        })

        return res.json(basket)
    }



    async removeFromFavourite(req, res, next) {
        const user = req.user;
        const {appartmentId} = req.body;

        try {
            await FavouriteApartment.destroy({
                where: {
                    favouriteId: user.id,
                    appartmentId: appartmentId
                }
            });

            return res.sendStatus(200);
        } catch (error) {
            return next(error);
        }
    }

    async removeAllFromFavourite(req, res, next) {
        const user = req.user;

        try {
            await FavouriteApartment.destroy({
                where: {
                    favouriteId: user.id
                }
            });

            return res.sendStatus(200);
        } catch (error) {
            return next(error);
        }
    }


}

module.exports = new FavouriteController()