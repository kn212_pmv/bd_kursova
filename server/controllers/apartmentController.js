const uuid = require('uuid')
const path = require('path')
const {Apartment, ApartmentInfo} = require('../models/models')
const ApiError = require('../error/apiError')


class ApartmentController {
    async create(req, res, next) {
        try {

            let {typeId, cityId, address, area, rooms, price, info} = req.body
            const {img} = req.files
            let fileName = uuid.v4() + ".jpg"
            img.mv(path.resolve(__dirname, '..', 'static', fileName))
            const apartment = await Apartment.create({typeId, cityId, address, area, rooms, price, img: fileName})

            if (info) {
                info = JSON.parse(info)
                info.forEach(i =>
                    ApartmentInfo.create({
                        title: i.title,
                        description: i.description,
                        appartmentId: apartment.id

                    }))
            }


            return res.json(apartment)
        } catch (e) {
            next(ApiError.badRequest(e.message))
        }

    }

    async getAll(req, res) {
        let {typeId, cityId, limit, page} = req.query
        page = page || 1
        limit = limit || 9
        let offset = page * limit - limit
        let apartments;
        if (!typeId && !cityId) {
            apartments = await Apartment.findAndCountAll({limit, offset})

        }
        if (typeId && !cityId) {
            apartments = await Apartment.findAndCountAll({where: {typeId}, limit, offset})

        }
        if (!typeId && cityId) {
            apartments = await Apartment.findAndCountAll({where: {cityId}, limit, offset})

        }
        if (typeId && cityId) {
            apartments = await Apartment.findAndCountAll({where: {cityId, typeId}, limit, offset})

        }
        return res.json(apartments)

    }

    async getOne(req, res) {
        const {id} = req.params
        const apartment = await Apartment.findOne(
            {
                where: {id},
                include: [{model: ApartmentInfo, as: 'info'}]
            }
        )
        return res.json(apartment)

    }

    async delete(req, res, next) {
        try {
            const {id} = req.params;

            // Проверяем, существует ли запись с указанным идентификатором
            const apartment = await Apartment.findByPk(id);
            if (!apartment) {
                return next(ApiError.badRequest(`Apartment with id ${id} not found`));
            }

            // Удаляем связанные записи ApartmentInfo, если они есть
            await ApartmentInfo.destroy({
                where: {appartmentId: id}
            });

            // Удаляем саму запись квартиры
            await Apartment.destroy({
                where: {id}
            });

            return res.json({message: 'Apartment deleted successfully'});
        } catch (e) {
            next(ApiError.badRequest(e.message));
        }
    }

    async update(req, res, next) {
        try {
            const { id } = req.params;
            let { typeId, cityId, address, area, rooms, price, info } = req.body;
            const { img } = req.files;

            // Проверяем, существует ли запись с указанным идентификатором
            let apartment = await Apartment.findByPk(id);
            if (!apartment) {
                return next(ApiError.badRequest(`Apartment with id ${id} not found`));
            }

            // Обновляем данные квартиры
            apartment.typeId = typeId || apartment.typeId;
            apartment.cityId = cityId || apartment.cityId;
            apartment.address = address || apartment.address;
            apartment.area = area || apartment.area;
            apartment.rooms = rooms || apartment.rooms;
            apartment.price = price || apartment.price;

            // Обновляем изображение, если оно было предоставлено
            if (img) {
                let fileName = uuid.v4() + ".jpg";
                img.mv(path.resolve(__dirname, '..', 'static', fileName));
                apartment.img = fileName;
            }

            // Обновляем связанные данные ApartmentInfo, если они были предоставлены
            if (info) {
                info = JSON.parse(info);
                await ApartmentInfo.destroy({ where: { appartmentId: apartment.id } });

                info.forEach(i =>
                    ApartmentInfo.create({
                        title: i.title,
                        description: i.description,
                        appartmentId: apartment.id
                    }));
            }

            // Сохраняем обновленные данные квартиры
            await apartment.save();

            return res.json(apartment);
        } catch (e) {
            next(ApiError.badRequest(e.message));
        }
    }

}

module.exports = new ApartmentController()