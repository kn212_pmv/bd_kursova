const {Type} = require('../models/models')
const ApiError = require('../error/apiError')
class TypeController {
    async create(req,res) {
        const {name} = req.body
        const type=await Type.create({name})
        return res.json(type)

    }
    async getAll(req,res) {
        const types = await Type.findAll()
        return res.json(types)

    }

    async delete(req, res, next) {
        try {
            const { id } = req.params;

            // Проверяем, существует ли тип с указанным идентификатором
            const type = await Type.findByPk(id);
            if (!type) {
                throw new ApiError.NotFound(`Type with id ${id} not found`);
            }

            // Удаляем тип
            await type.destroy();

            return res.json({ message: 'Type deleted successfully' });
        } catch (e) {
            next(ApiError.badRequest(e.message));
        }
    }

    async update(req, res, next) {
        try {
            const { id } = req.params;
            const { name } = req.body;

            // Проверяем, существует ли тип с указанным идентификатором
            let type = await Type.findByPk(id);
            if (!type) {
                throw new ApiError.NotFound(`Type with id ${id} not found`);
            }

            // Обновляем имя типа
            type.name = name || type.name;

            // Сохраняем обновленные данные типа
            await type.save();

            return res.json(type);
        } catch (e) {
            next(ApiError.badRequest(e.message));
        }
    }

}

module.exports = new TypeController()