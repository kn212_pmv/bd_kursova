const {City} = require("../models/models");
const ApiError = require('../error/apiError')
class CityController {
    async create(req,res) {
        const {name} = req.body
        const city=await City.create({name})
        return res.json(city)
    }
    async getAll(req,res) {
        const city = await City.findAll()
        return res.json(city)

    }

    async delete(req, res, next) {
        try {
            const { id } = req.params;

            // Проверяем, существует ли запись с указанным идентификатором
            const city = await City.findByPk(id);
            if (!city) {
                return next(ApiError.badRequest(`City with id ${id} not found`));
            }

            // Удаляем запись города
            await City.destroy({
                where: { id }
            });

            return res.json({ message: 'City deleted successfully' });
        } catch (e) {
            next(ApiError.badRequest(e.message));
        }
    }

    async update(req, res, next) {
        try {
            const { id } = req.params;
            const { name } = req.body;

            // Проверяем, существует ли запись с указанным идентификатором
            let city = await City.findByPk(id);
            if (!city) {
                return next(ApiError.badRequest(`City with id ${id} not found`));
            }

            // Обновляем имя города
            city.name = name || city.name;

            // Сохраняем обновленные данные города
            await city.save();

            return res.json(city);
        } catch (e) {
            next(ApiError.badRequest(e.message));
        }
    }



}

module.exports = new CityController()