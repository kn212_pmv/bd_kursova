const {exec} = require('child_process');

class backupController {
    async createBackup(req, res, next) {
        try {
            // Виконуємо команду для створення бекапу бази даних
            const password = process.env.DB_PASSWORD;
            const command = `pg_dump -U ${process.env.DB_USER} -d ${process.env.DB_NAME} -F c -b -v -p ${process.env.DB_PORT} -f D:\\backup\save`;

            exec(command, { env: { ...process.env, PGPASSWORD: password } }, (error, stdout, stderr) => {
                if (error) {
                    console.error('Помилка при створенні бекапу:', error);
                    return res.status(501).json({message: 'Internal Server Error', error: error.message});
                }
                console.log('Бекап створено успішно:', stdout);
                return res.status(200).json({ message: 'Бекап створено успішно' });
            });
        } catch (error) {
            console.error(error);
            return res.status(501).json({message: 'Internal Server Error', error: error.message});
        }
    };
}

module.exports = new backupController()